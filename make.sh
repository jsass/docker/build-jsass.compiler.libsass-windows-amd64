#!/bin/bash

set -xeuo pipefail

cd /jsass/src/main

rm -fr resources/windows-x64
mkdir -p resources/windows-x64

# *** Build libsass

make -C libsass clean
cd libsass
git reset --hard # hard reset
git clean -xdf # hard clean
cd ..

# We use:
# MAKE=mingw32                      to make sure we build for windows
# CC=i686-w64-mingw32-gcc           cross compile with mingw32-gcc compiler
# CXX=i686-w64-mingw32-g++          cross compile with mingw32-g++ compiler
# WINDRES=i686-w64-mingw32-windres  cross compile with mingw32-windres
# BUILD="static"                    to make sure that we build a static library
MAKE=mingw32 \
CC=x86_64-w64-mingw32-gcc \
CXX=x86_64-w64-mingw32-g++ \
WINDRES=x86_64-w64-mingw32-windres \
BUILD=static \
    make -C libsass -j4 lib/libsass.dll
cp libsass/lib/libsass.dll resources/windows-x64/libsass.dll

# *** Build libjsass

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-mingw64-amd64.cmake ../
make
cp libjsass.dll ../../resources/windows-x64/libjsass.dll

# *** Build libjsass_test

cd /jsass/src/test

rm -fr resources/windows-x64
mkdir -p resources/windows-x64

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-mingw64-amd64.cmake ../
make
cp libjsass_test.dll ../../resources/windows-x64/libjsass_test.dll
